import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';

/**
 * Generated class for the TravelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-travel',
  templateUrl: 'travel.html',
})
export class TravelPage {

  constructor(public toastCtrl: ToastController, public loadingCtrl: LoadingController ,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TravelPage');
  }

  ToastPosition(position: string){
    console.log(position);
    let tp = this.toastCtrl.create({
      message: 'toast = '+position,
      position: position,
      duration: 2500
    });

    tp.present();
  }

  Toast(){
    let toast = this.toastCtrl.create({
      message: 'สวัสดี Toast',
      duration: 2000,
      showCloseButton: true,
      closeButtonText: 'ตกลง'
    });

    toast.present();
  }

  Loading(){
    console.log('Load');
    this.loadingCtrl.create({
      content: 'โปรดรอสักครู่',
      duration: 3000,
      dismissOnPageChange: true
    }).present();
  }

}
