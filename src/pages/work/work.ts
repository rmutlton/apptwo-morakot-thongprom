import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CartPage } from "../cart/cart";
import { MedicalPage } from "../medical/medical";
import { CafePage } from "../cafe/cafe";

/**
 * Generated class for the WorkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-work',
  templateUrl: 'work.html',
})
export class WorkPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WorkPage');
  }

  cart(){
    console.log("ห้างสรรพสินค้า");
    this.navCtrl.push(CartPage, {
      company1: 'เซ็นทรัล',
      company2: 'บิ๊กซี',
      company3: 'โลตัส',
      company4: 'แม็คโคร'
    });
  }

  medical(){
    console.log("โรงพยาบาล");
    this.navCtrl.push(MedicalPage ,{
      hos1: 'โรงพยาบาลนเรศวร',
      hos2: 'โรงพยาบาลพิษณุเวช',
      hos3: 'โรงพยาบาลกรุงเทพ'
    });
  }

  cafe(){
    console.log("ร้านกาแฟ");
    this.navCtrl.push(CafePage, {
      cafe1: 'Amezon',
      cafe2: 'Starbucks',
      cafe3: 'ดอยช้าง'
    });
  }

}
