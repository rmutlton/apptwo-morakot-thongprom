import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController) {
  }

  radioAlert(){
    let radio = this.alertCtrl.create();

    radio.setTitle('เลือกชั้นปี ?');

    radio.addInput({
      type: 'radio',
      label: 'ปี 1',
      value: '1',
      checked: true
    });

    radio.addInput({
      type: 'radio',
      label: 'ปี 2',
      value: '2',
    });

    radio.addInput({
      type: 'radio',
      label: 'ปี 3',
      value: '3',
    });

    radio.addInput({
      type: 'radio',
      label: 'ปี 4',
      value: '4',
    });

    radio.addButton('ยกเลิก');
    radio.addButton({
      text: 'ตกลง',
      handler: data => {
        console.log('data = ', data);
      }
    });

    radio.present();

  }

  checkAlert() {
    let checkalert = this.alertCtrl.create();

    checkalert.setTitle('กรุณาเลือก สี ?');

    checkalert.addInput({
      type: 'checkbox',
      label: 'สีแดง',
      value: 'red',
      //checked: true
    });

    checkalert.addInput({
      type: 'checkbox',
      label: 'สีเหลือง',
      value: 'yellow',
    });

    checkalert.addInput({
      type: 'checkbox',
      label: 'สีเขียว',
      value: 'green',
    });

    checkalert.addInput({
      type: 'checkbox',
      label: 'สีน้ำเงิน',
      value: 'blue',
    });

    checkalert.addButton('ยกเลิก');
    checkalert.addButton({
      text: 'ตกลง',
      handler: data => {
        console.log('data = ', data);



      }
    });

    checkalert.present();

  }

  doAlert() {
    let alert = this.alertCtrl.create({
      title: 'คำเตือน',
      subTitle: 'คุณได้ทำการกดปุ่ม Click แล้ว',
      buttons: ['OK']
    });
    alert.present();
  }

  conAlert(){
    let confirm = this.alertCtrl.create({
      title: 'สมัครสมาชิก',
      message: 'คุณต้องการสมัครสมาชิก ใช่ หรือ ไม่ ?',
      buttons: [
        {
          text: 'ไม่',
          handler: () => {
            console.log('ไม่สมัครสมาชิก');
          }
        },
        {
          text: 'ใช่',
          handler: () => {
            console.log('สมัครสมาชิกเรียบร้อย');
          }
        }
      ]
    });
    confirm.present();
  }

  saveAlert(){
    let save = this.alertCtrl.create({
      title: 'บันทึกข้อมูล',
      message: 'กรุณาใส่ข้อความที่ต้องการบันทึก',
      inputs: [
        {
          name: 'data',
          placeholder: 'กรุณากรอกข้อมูล'
        }
      ],
      buttons: [
        {
          text: 'ยกเลิก',
          handler: data => {
            console.log("ยกเลิก");
          }
        },
        {
          text: 'บันทึก',
          handler: data => {
            console.log(data['data']);
          }
        }
      ]
    });
    save.present();
  }

}
