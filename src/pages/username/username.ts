import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { ListregisterServiceProvider } from "../../providers/listregister-service/listregister-service";
import { Listregister } from "../../models/Listregister";

import { Subscription } from "rxjs/Subscription";

import { RegisterServiceProvider } from "../../providers/register-service/register-service";
import { FeedBack } from "../../models/feedback";

import { EdituserPage } from "../edituser/edituser";

@IonicPage()
@Component({
  selector: 'page-username',
  templateUrl: 'username.html',
})
export class UsernamePage {

  listregister: Listregister[];
  sub:Subscription;

  feedback:FeedBack;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private listregisterServiceProvider:ListregisterServiceProvider,
    private registerServiceProvider:RegisterServiceProvider,
    private loadingCtrl:LoadingController,
    private alertCtrl:AlertController
    ) {
  }

  private getListregister(){
    this.sub = this.listregisterServiceProvider.getListregister().subscribe(
      (listregister:Listregister[]) => this.listregister = listregister
    )
  }

  deleteRegister(id,data){
    console.log(id);
    
    let alertconfirm = this.alertCtrl.create({
      title: 'ยืนยันการลบรายชื่อ',
      message: 'คุณต้องการลบรายชื่อ ใช่ หรือ ไม่',
      buttons: [
        {
          text: 'ยืนยัน',
          handler: () => {

            let loader =this.loadingCtrl.create({
              content: "กำลังลบข้อมูล"
            });
        
            loader.present();

            this.registerServiceProvider.delregis(id).subscribe(
              (feedback:FeedBack) => {
                this.feedback = feedback
                if(this.feedback.status === 'ok'){
                  let alert = this.alertCtrl.create({
                    title: this.feedback.message,
                    buttons: ['ตกลง']
                  })
                  alert.present();
        
                  let index = this.listregister.indexOf(data);
        
                  if(index > -1){
                    this.listregister.splice(index, 1);
                  }
        
                }
              },(msg) => {
                loader.dismiss();
              },() => {
                loader.dismiss();
              }
            )
          }
        },
        {
          text: 'ยกเลิก',
          handler: () => {
           
          }
        }
      ]
    })

    alertconfirm.present();

    

  }

  editRegister(id,data){
    console.log(id);
    console.log(data);
    this.navCtrl.push(EdituserPage,{
      id: id,
      data: data
    });
  }

  ionViewWillEnter(){
    this.getListregister();
  }

  ionViewWillLeave(){
    this.sub.unsubscribe();
  }

}
