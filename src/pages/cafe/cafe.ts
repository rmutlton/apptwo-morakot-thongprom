import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CafePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cafe',
  templateUrl: 'cafe.html',
})
export class CafePage {

  cafe1:string
  cafe2:string
  cafe3:string

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.cafe1 = this.navParams.get('cafe1');
    this.cafe2 = this.navParams.get('cafe2');
    this.cafe3 = this.navParams.get('cafe3');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CafePage');
  }

}
