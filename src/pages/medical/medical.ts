import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MedicalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-medical',
  templateUrl: 'medical.html',
})
export class MedicalPage {

  hos1:string
  hos2:string
  hos3:string

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.hos1 = this.navParams.get('hos1');
    this.hos2 = this.navParams.get('hos2');
    this.hos3 = this.navParams.get('hos3');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicalPage');
  }

}
