import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { WorkPage } from "../work/work";
import { TravelPage } from "../travel/travel";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  WorkRoot = WorkPage;
  TravelRoot = TravelPage;

  constructor() {

  }
}
