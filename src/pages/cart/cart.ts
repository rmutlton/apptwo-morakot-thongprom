import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  company1:string;
  company2:string;
  company3:string;
  company4:string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.company1 = this.navParams.get('company1');
    this.company2 = this.navParams.get('company2');
    this.company3 = this.navParams.get('company3');
    this.company4 = this.navParams.get('company4');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }

}
