import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { RegisterServiceProvider } from "../../providers/register-service/register-service";
import { FeedBack } from "../../models/feedback";

@IonicPage()
@Component({
  selector: 'page-edituser',
  templateUrl: 'edituser.html',
})
export class EdituserPage {

  feedback:FeedBack;
  id:string;
  data:any;
  todo = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private registerServiceProvider:RegisterServiceProvider,
    private loadingCtrl:LoadingController,
    private alertCtrl:AlertController
    ) {
    this.id = this.navParams.get('id');
    this.data = this.navParams.get('data');
  }

  editForm(id){
    let username = this.data['username'];
    let firstname = this.data['firstname'];
    let lastname = this.data['lastname'];
    let tel = this.data['tel'];

    let loader = this.loadingCtrl.create({
      content: "กำลังแก้ไขข้อมูล"
    });

    loader.present();

    this.registerServiceProvider.editregis(id,username,firstname,lastname,tel).subscribe(
      (feedback:FeedBack) => {
        this.feedback = feedback;
        if(this.feedback.status === 'ok'){
          let alert = this.alertCtrl.create({
            title: this.feedback.message,
            buttons: ['ตกลง']
          });
          alert.present();
        } else {
          let alert = this.alertCtrl.create({
            title: this.feedback.message,
            buttons: ['ตกลง']
          });
          alert.present();
        }
      },(msg) => {
        console.log(msg);
        loader.dismiss();
      },() => {
        loader.dismiss();
      }
    );

    console.log(id);
    console.log(username);
    console.log(firstname);
    console.log(lastname);
    console.log(tel);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EdituserPage');
  }

}
