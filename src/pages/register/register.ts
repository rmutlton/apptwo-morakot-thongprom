import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { RegisterServiceProvider } from "../../providers/register-service/register-service";
import { FeedBack } from "../../models/feedback";

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  feedback:FeedBack

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private registerServiceProvider:RegisterServiceProvider,
    private loadingCtrl:LoadingController,
    private alertCtrl:AlertController
    ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  todo = {}

  logForm(){
    console.log(this.todo);
    let username = this.todo['username'];
    let password = this.todo['password'];
    let firstname = this.todo['firstname'];
    let lastname = this.todo['lastname'];
    let tel = this.todo['tel'];

    let loader = this.loadingCtrl.create({
      content: "กำลังบันทึกข้อมูล"
    });

    loader.present();

    this.registerServiceProvider.regis(username,password,firstname,lastname,tel).subscribe(
      (feedback:FeedBack) => {
        this.feedback = feedback;
        if(this.feedback.status === 'ok'){
          let alert = this.alertCtrl.create({
            title: this.feedback.message,
            buttons: ['ตกลง']
          });

          alert.present();
        }
      },(msg)=>{
        console.log(msg)
        loader.dismiss();
      },()=>{
        loader.dismiss();
      }

    );

  }

}
