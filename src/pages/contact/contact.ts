import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  name:string;
  lastname:string;
  address:string;
  tel:string = '';
  
  constructor(public navCtrl: NavController) {
    this.name = 'มรกต';
    this.lastname = 'ทองพรหม';
    this.address = 'พิษณุโลก';
  }

  showTel(){
    
    if(this.tel == ''){
      this.tel = '080-5151651';
    } else {
      this.tel = '';
    }

  }

}
