export interface Listregister {
    id: string;
    username: string;
    password: string;
    firstname: string;
    lastname: string;
    tel: string;
}