import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { WorkPage } from "../pages/work/work";
import { TravelPage } from "../pages/travel/travel";
import { CartPage } from "../pages/cart/cart";
import { MedicalPage } from "../pages/medical/medical";
import { CafePage } from "../pages/cafe/cafe";
import { FinancePage } from "../pages/finance/finance";
import { UsernamePage } from "../pages/username/username";
import { RegisterPage } from "../pages/register/register";
import { EdituserPage } from "../pages/edituser/edituser";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HTTP } from "@ionic-native/http";
import { HttpClientModule } from '@angular/common/http';
import { RegisterServiceProvider } from '../providers/register-service/register-service';
import { ListregisterServiceProvider } from '../providers/listregister-service/listregister-service';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    WorkPage,
    TravelPage,
    CartPage,
    MedicalPage,
    CafePage,
    FinancePage,
    UsernamePage,
    RegisterPage,
    EdituserPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    WorkPage,
    TravelPage,
    CartPage,
    MedicalPage,
    CafePage,
    FinancePage,
    UsernamePage,
    RegisterPage,
    EdituserPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RegisterServiceProvider,
    ListregisterServiceProvider
  ]
})
export class AppModule {}
