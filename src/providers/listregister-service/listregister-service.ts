import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Listregister } from "../../models/Listregister";
import { Observable } from "rxjs/Observable";

@Injectable()
export class ListregisterServiceProvider {

  apiUrl:string = 'http://10.90.160.91/ionic-api/select_register.php';

  constructor(public http: HttpClient) {
    console.log('Hello ListregisterServiceProvider Provider');
  }

  getListregister(): Observable<Listregister[]> {
    return this.http.get<Listregister[]>(this.apiUrl);
  }

}
