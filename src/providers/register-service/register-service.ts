import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { FeedBack } from "../../models/feedback";

@Injectable()
export class RegisterServiceProvider {

  apiURL:string = 'http://10.90.160.91/ionic-api/insert_register.php';
  apiURLdel:string = 'http://10.90.160.91/ionic-api/delete_register.php';
  apiURLedit:string = 'http://10.90.160.91/ionic-api/edit_register.php';

  constructor(public http: HttpClient) {
    console.log('Hello RegisterServiceProvider Provider');
  }

  public regis(username:string, password:string, firstname:string, lastname:string, tel:string):
  Observable<FeedBack> {
    const header = { 'Content-Type': 'application/json' };
    let data = {
      'username': username,
      'password': password,
      'firstname': firstname,
      'lastname': lastname,
      'tel': tel
    }
    return this.http.post<FeedBack>(this.apiURL, data, {headers: header})
  }

  public editregis(id:string, username:string, firstname:string, lastname:string, tel:string):
  Observable<FeedBack> {
    const header = { 'Content-Type': 'application/json' };
    let data = {
      'id': id,
      'username': username,
      'firstname': firstname,
      'lastname': lastname,
      'tel': tel
    }
    return this.http.post<FeedBack>(this.apiURLedit, data, {headers: header})
  }

  public delregis(id:string):
  Observable<FeedBack> {
    const header = { 'Content-Type': 'application/json' };
    let data = {
      'id': id
    }
    return this.http.post<FeedBack>(this.apiURLdel, data, {headers: header})
  }

}
